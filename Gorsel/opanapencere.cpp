#include "opanapencere.h"
#include "ui_opanapencere.h"

#include <QMessageBox>

#include <Formlar/VeriGiris/opogrenciformu.h>
#include <Formlar/VeriGiris/opogretmenformu.h>
#include <Formlar/VeriGiris/opdersformu.h>
#include <Formlar/VeriGiris/opnotformu.h>
#include <Formlar/VeriGiris/opsinifformu.h>

OPAnaPencere::OPAnaPencere(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::OPAnaPencere)
{
    ui->setupUi(this);
}

OPAnaPencere::~OPAnaPencere()
{
    delete ui;
}

void OPAnaPencere::close() {
  auto cevap =  QMessageBox::question(this, "ÇIKIŞ", "Çıkmak İstediğinize Emin misiniz?",
    QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
  if (cevap == QMessageBox::Yes) {
  QMainWindow::close();
   }
 }


void OPAnaPencere::on_actionYeni_Ogrenci_Ekle_triggered() {
        OPOgrenciFormu form;
        form.exec();
    }


void OPAnaPencere::on_actionYeni_retmen_Ekle_triggered() {
        OPOgretmenFormu form;
        form.exec();
    }

void OPAnaPencere::on_actionYeni_Not_Ekle_triggered() {
        OPNotFormu form;
        form.exec();
    }


void OPAnaPencere::on_actionYeni_Ders_Ekle_triggered() {
        OPDersFormu form;
        form.exec();
    }


void OPAnaPencere::on_actionYeni_Sinif_Olustur_triggered() {
        OPSinifFormu form;
        form.exec();
}

#include "opanapencere.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    OPAnaPencere w;
    w.show();
    return a.exec();
}

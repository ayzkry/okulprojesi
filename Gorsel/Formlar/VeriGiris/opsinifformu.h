#ifndef OPSINIFFORMU_H
#define OPSINIFFORMU_H

#include <QDialog>

#include <VeriSiniflari/sinif.h>

namespace Ui {
class OPSinifFormu;
}

class OPSinifFormu : public QDialog
{
    Q_OBJECT

public:
    explicit OPSinifFormu(QWidget *parent = nullptr);
    ~OPSinifFormu();

private:
    Ui::OPSinifFormu *ui;
};

#endif // OPSINIFFORMU_H

#ifndef OPOGRENCIFORMU_H
#define OPOGRENCIFORMU_H

#include <QDialog>

#include <VeriSiniflari/ogrenci.h>

namespace Ui {
class OPOgrenciFormu;
}

class OPOgrenciFormu : public QDialog
{
    Q_OBJECT

public:
    explicit OPOgrenciFormu(QWidget *parent = nullptr);
    ~OPOgrenciFormu();

private:
    Ui::OPOgrenciFormu *ui;
};

#endif // OPOGRENCIFORMU_H

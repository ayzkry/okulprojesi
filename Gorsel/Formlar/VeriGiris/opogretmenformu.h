#ifndef OPOGRETMENFORMU_H
#define OPOGRETMENFORMU_H

#include <QDialog>

#include <VeriSiniflari/ogretmen.h>

namespace Ui {
class OPOgretmenFormu;
}

class OPOgretmenFormu : public QDialog
{
    Q_OBJECT

public:
    explicit OPOgretmenFormu(QWidget *parent = nullptr);
    ~OPOgretmenFormu();

private:
    Ui::OPOgretmenFormu *ui;
};

#endif // OPOGRETMENFORMU_H

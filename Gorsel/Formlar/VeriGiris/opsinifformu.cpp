#include "opsinifformu.h"
#include "ui_opsinifformu.h"

OPSinifFormu::OPSinifFormu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OPSinifFormu)
{
    ui->setupUi(this);
}

OPSinifFormu::~OPSinifFormu()
{
    delete ui;
}

#ifndef OPDERSFORMU_H
#define OPDERSFORMU_H

#include <QDialog>

#include <VeriSiniflari/ders.h>

namespace Ui {
class OPDersFormu;
}

class OPDersFormu : public QDialog
{
    Q_OBJECT

public:
    explicit OPDersFormu(QWidget *parent = nullptr);
    ~OPDersFormu();

private:
    Ui::OPDersFormu *ui;

    Dersler::Ptr _ders;
};

#endif // OPDERSFORMU_H

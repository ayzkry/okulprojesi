#ifndef OPNOTFORMU_H
#define OPNOTFORMU_H

#include <QDialog>

#include <VeriSiniflari/notlar.h>

namespace Ui {
class OPNotFormu;
}

class OPNotFormu : public QDialog
{
    Q_OBJECT

public:
    explicit OPNotFormu(QWidget *parent = nullptr);
    ~OPNotFormu();

private:
    Ui::OPNotFormu *ui;
};

#endif // OPNOTFORMU_H

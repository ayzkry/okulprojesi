#ifndef OPANAPENCERE_H
#define OPANAPENCERE_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class OPAnaPencere; }
QT_END_NAMESPACE

class OPAnaPencere : public QMainWindow
{
    Q_OBJECT

public:
    OPAnaPencere(QWidget *parent = nullptr);
    ~OPAnaPencere();

public slots:
    void close();

private slots:
    void on_actionYeni_Ogrenci_Ekle_triggered();

    void on_actionYeni_retmen_Ekle_triggered();

    void on_actionYeni_Not_Ekle_triggered();

    void on_actionYeni_Ders_Ekle_triggered();

    void on_actionYeni_Sinif_Olustur_triggered();

private:
    Ui::OPAnaPencere *ui;
};
#endif // OPANAPENCERE_H

#ifndef DERSVERIYONETICISI_H
#define DERSVERIYONETICISI_H

#include "temelveriyonetimsinifi.h"
#include <QObject>
#include <VeriSiniflari/ders.h>

class dersveriyoneticisi : public QObject,
                           public temelveriyonetimsinifi<Dersler> {
    Q_OBJECT
public:
    explicit dersveriyoneticisi(QObject *parent = nullptr);

signals:
  virtual void elemanEklendi(Pointer ptr);
  virtual void elemenSilindi(Pointer ptr);
  virtual void elemanDegisti(Pointer eski, Pointer yeni);
};

#endif // DERSVERIYONETICISI_H

#ifndef OGRENCIVERIYONETICISI_H
#define OGRENCIVERIYONETICISI_H

#include "temelveriyonetimsinifi.h"
#include <QObject>
#include <VeriSiniflari/ogrenci.h>

class ogrenciveriyoneticisi : public QObject,
                           public temelveriyonetimsinifi<Ogrenci> {
    Q_OBJECT
public:
    explicit ogrenciveriyoneticisi(QObject *parent = nullptr);

signals:
  virtual void elemanEklendi(Pointer ptr);
  virtual void elemenSilindi(Pointer ptr);
  virtual void elemanDegisti(Pointer eski, Pointer yeni);
};

#endif // OGRENCIVERIYONETICISI_H

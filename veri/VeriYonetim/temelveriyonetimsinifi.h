#ifndef TEMELVERIYONETIMSINIFI_H
#define TEMELVERIYONETIMSINIFI_H

#include <QVector>
#include <functional>

template <class T> class temelveriyonetimsinifi {
public:
  typedef T Veri;
  typedef typename T::Ptr Pointer;
  typedef typename T::IdTuru IdTuru;
  typedef typename T::IsaretsizTamsayi IsaretsizTamsayi;
  typedef typename T::Tamsayi Tamsayi;
  typedef QVector<Pointer> Liste;
  typedef std::function<bool(Pointer)> FiltreFonksiyonu;

public:
  temelveriyonetimsinifi() {}

  Pointer yeni() { return T::yeni(); }

  void ekle(Pointer veri) {

    FiltreFonksiyonu fonk = [veri](Pointer v) -> bool {
      return v->id() == veri->id();
    };
    IsaretsizTamsayi es = this->filtreyeUyanElemanSayisi(fonk);
    if (es == 0) {
      _veriler.append(veri->kopyala());
      elemanEklendi(veri);
    }
  }

  void duzenle(Pointer eski, Pointer yeni) {
    for (int i = 0; i < _veriler.size(); i++) {
      if (eski->id() == _veriler[i]->id()) {
        _veriler[i] = yeni;
      }
    }
  }

  void sil(Pointer silinecek) {
    for (int i = 0; i < _veriler.size(); i++) {
      if (silinecek->id() == _veriler[i]->id()) {
        sil(i);
        return;
      }
    }
  }

  void sil(IsaretsizTamsayi idx) {
    Pointer ptr = _veriler.takeAt(idx);
    elemanSilindi(ptr);
  }

  Liste ara(FiltreFonksiyonu f) const {
    Liste sonuc;
    for (auto eleman : _veriler) {
      if (f(eleman)) {
        sonuc.append(eleman->kopyala());
      }
    }
    return sonuc;
  }

  Pointer ilkiniBul(FiltreFonksiyonu f) const {
    for (auto eleman : _veriler) {
      if (f(eleman)) {
        return eleman->kopyala();
      }
    }
    Pointer sonuc(nullptr);
    return sonuc;
  }

  Pointer sonuncuyuBul(FiltreFonksiyonu f) const {
    for (auto eleman = _veriler.rbegin(); eleman != _veriler.rend(); eleman++) {
      if (f(*eleman)) {
        return (*eleman)->kopyala();
      }
    }
    Pointer sonuc(nullptr);
    return sonuc;
  }

  IsaretsizTamsayi filtreyeUyanElemanSayisi(FiltreFonksiyonu f) const {
    IsaretsizTamsayi sonuc = 0;
    for (auto eleman : _veriler) {
      if (f(eleman)) {
        sonuc++;
      }
    }
    return sonuc;
  }

  virtual void elemanEklendi(Pointer ptr) = 0;
  virtual void elemanDegisti(Pointer eski, Pointer yeni) = 0;

protected:
  Liste _veriler;
};

#endif // TEMELVERIYONETIMSINIFI_H

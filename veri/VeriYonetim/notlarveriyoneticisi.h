#ifndef NOTLARVERIYONETICISI_H
#define NOTLARVERIYONETICISI_H

#include "temelveriyonetimsinifi.h"
#include <QObject>
#include <VeriSiniflari/notlar.h>

class notlarveriyoneticisi : public QObject,
                           public temelveriyonetimsinifi<notlar> {
    Q_OBJECT
public:
    explicit notlarveriyoneticisi(QObject *parent = nullptr);

signals:
  virtual void elemanEklendi(Pointer ptr);
  virtual void elemenSilindi(Pointer ptr);
  virtual void elemanDegisti(Pointer eski, Pointer yeni);
};

#endif // NOTLARVERIYONETICISI_H

#ifndef SINIFVERIYONETICISI_H
#define SINIFVERIYONETICISI_H

#include "temelveriyonetimsinifi.h"
#include <QObject>
#include <VeriSiniflari/sinif.h>

class sinifveriyoneticisi : public QObject,
                            public temelveriyonetimsinifi<Sinif> {
    Q_OBJECT
public:
    explicit sinifveriyoneticisi(QObject *parent = nullptr);

signals:
  virtual void elemanEklendi(Pointer ptr);
  virtual void elemenSilindi(Pointer ptr);
  virtual void elemanDegisti(Pointer eski, Pointer yeni);
};

#endif // SINIFVERIYONETICISI_H

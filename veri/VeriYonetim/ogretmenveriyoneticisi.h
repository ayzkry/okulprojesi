#ifndef OGRETMENVERIYONETICISI_H
#define OGRETMENVERIYONETICISI_H

#include "temelveriyonetimsinifi.h"
#include <QObject>
#include <VeriSiniflari/ogretmen.h>

class ogretmenveriyoneticisi : public QObject,
                               public temelveriyonetimsinifi<ogretmen> {
    Q_OBJECT
public:
    explicit ogretmenveriyoneticisi(QObject *parent = nullptr);

signals:
  virtual void elemanEklendi(Pointer ptr);
  virtual void elemenSilindi(Pointer ptr);
  virtual void elemanDegisti(Pointer eski, Pointer yeni);
};

#endif // OGRETMENVERIYONETICISI_H

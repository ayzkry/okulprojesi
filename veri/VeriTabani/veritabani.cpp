#include "veritabani.h"

VeriTabani::VeriTabani(QObject *parent) : QObject(parent){

}

VeriTabani &VeriTabani::veritabani()
{
    static VeriTabani nesne;
    return nesne;
}

dersveriyoneticisi &VeriTabani::ders()
{
    return _ders;
}

notlarveriyoneticisi &VeriTabani::notlar()
{
    return _notlar;
}

ogrenciveriyoneticisi &VeriTabani::ogrenci()
{
    return _ogrenci;
}

ogretmenveriyoneticisi &VeriTabani::ogretmen()
{
    return _ogretmen;
}

sinifveriyoneticisi &VeriTabani::sinif()
{
    return _sinif;
}



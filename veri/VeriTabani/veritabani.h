#ifndef VERITABANI_H
#define VERITABANI_H

#include <QObject>

#include <VeriYonetim/dersveriyoneticisi.h>
#include <VeriYonetim/notlarveriyoneticisi.h>
#include <VeriYonetim/ogrenciveriyoneticisi.h>
#include <VeriYonetim/ogretmenveriyoneticisi.h>
#include <VeriYonetim/sinifveriyoneticisi.h>

class VeriTabani : public QObject
{
    Q_OBJECT

private:
    explicit VeriTabani(QObject *parent = nullptr);

public:
        static VeriTabani &veritabani();

        dersveriyoneticisi &ders();

        notlarveriyoneticisi &notlar();

        ogrenciveriyoneticisi &ogrenci();

        ogretmenveriyoneticisi &ogretmen();

        sinifveriyoneticisi &sinif();

private:
        dersveriyoneticisi _ders;
        notlarveriyoneticisi _notlar;
        ogrenciveriyoneticisi _ogrenci;
        ogretmenveriyoneticisi _ogretmen;
        sinifveriyoneticisi _sinif;

signals:

};
















#endif // VERITABANI_H

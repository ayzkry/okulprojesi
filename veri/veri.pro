QT -= gui

TEMPLATE = lib
DEFINES += VERI_LIBRARY

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    VeriSiniflari/ders.cpp \
    VeriSiniflari/notlar.cpp \
    VeriSiniflari/ogrenci.cpp \
    VeriSiniflari/ogretmen.cpp \
    VeriSiniflari/sinif.cpp \
    VeriSiniflari/temelverisinifi.cpp \
    VeriTabani/veritabani.cpp \
    VeriYonetim/dersveriyoneticisi.cpp \
    VeriYonetim/notlarveriyoneticisi.cpp \
    VeriYonetim/ogrenciveriyoneticisi.cpp \
    VeriYonetim/ogretmenveriyoneticisi.cpp \
    VeriYonetim/sinifveriyoneticisi.cpp \
    veri.cpp

HEADERS += \
    VeriSiniflari/ders.h \
    VeriSiniflari/notlar.h \
    VeriSiniflari/ogrenci.h \
    VeriSiniflari/ogretmen.h \
    VeriSiniflari/sinif.h \
    VeriSiniflari/temelverisinifi.h \
    VeriTabani/veritabani.h \
    VeriYonetim/dersveriyoneticisi.h \
    VeriYonetim/notlarveriyoneticisi.h \
    VeriYonetim/ogrenciveriyoneticisi.h \
    VeriYonetim/ogretmenveriyoneticisi.h \
    VeriYonetim/sinifveriyoneticisi.h \
    VeriYonetim/temelveriyonetimsinifi.h \
    veri_global.h \
    veri.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

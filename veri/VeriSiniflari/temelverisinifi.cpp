#include "temelverisinifi.h"

TemelVeriSinifi::TemelVeriSinifi(QObject *parent) : QObject(parent) {}

TemelVeriSinifi::IdTuru TemelVeriSinifi::id() const { return _id; }

void TemelVeriSinifi::setId(const IdTuru &id) {
  if (_id != id) {
    _id = id;
    idDegisti(_id);
  }
}

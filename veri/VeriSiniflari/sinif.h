#ifndef SINIF_H
#define SINIF_H

#include "temelverisinifi.h"

class Sinif : public TemelVeriSinifi {
    Q_OBJECT

public:
    typedef Sinif Veri;
    typedef std::shared_ptr<Veri> Ptr;

public:
  explicit Sinif(QObject *parent = nullptr);

  int sinifid() const;
  void setSinifid(int sinifid);

  QString sinifAdi() const;
  void setSinifAdi(const QString &sinifAdi);

  int ogretmenid() const;
  void setOgretmenid(int ogretmenid);

  int yil() const;
  void setYil(int yil);

signals:
  void sinifidDegisti(int sinifid);
  void sinifAdiDegisti(const QString &sinifAdi);
  void ogretmenidDegisti(int ogretmenid);
  void yilDegisti(int yil);

private:
  int _sinifid;
  QString _sinifAdi;
  int _ogretmenid;
  int _yil;
};

#endif // SINIF_H

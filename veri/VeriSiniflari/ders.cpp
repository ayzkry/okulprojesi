#include "ders.h"
#include <QVector>
#include <QtMath>

Dersler::Dersler(QObject *parent) : TemelVeriSinifi(parent) {
    _dersid = 0;
    _dersAdi = "";
    _dersYili = 0;
    _donem = 0;
}

int Dersler::dersid() const { return _dersid; }

void Dersler::setdersId(int dersid) {
  if(_dersid != dersid) {
    _dersid = dersid;
    dersidDegisti(_dersid);
    }
}

QString Dersler::dersAdi() const { return _dersAdi; }

void Dersler::setDersAdi(const QString &dersAdi) {
  if(_dersAdi != dersAdi) {
    _dersAdi = dersAdi;
    dersAdiDegisti(_dersAdi);
  }
}

int Dersler::dersYili() const { return _dersYili; }

void Dersler::setDersYili(int dersYili) {
  if (_dersYili != dersYili) {
     _dersYili = dersYili;
     dersYiliDegisti(_dersYili);
  }
}

int Dersler::donem() const { return _donem; }

void Dersler::setDonem(int donem) {
  if (_donem != donem) {
     _donem = donem;
     donemDegisti(_donem);
  }
}

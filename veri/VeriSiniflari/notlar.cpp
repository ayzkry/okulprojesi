#include "notlar.h"
#include <QtMath>

notlar::notlar(QObject *parent) : TemelVeriSinifi(parent) {}

int notlar::notid() const { return _notid; }

void notlar::setnotId(int notid) {
  if (_notid != notid) {
    _notid = notid;
    notidDegisti(_notid);
  }
}

int notlar::dersid() const { return _dersid; }

void notlar::setDersid(int dersid) {
  if (_dersid != dersid) {
    _dersid = dersid;
    dersidDegisti(_dersid);
  }
}

int notlar::ogrid() const { return _ogrid; }

void notlar::setOgrid(int ogrid) {
  if (_ogrid != ogrid) {
    _ogrid = ogrid;
    ogridDegisti(_ogrid);
  }
}

int notlar::Not() const { return _Not; }

void notlar::setNot(int Not) {
  if (_Not != Not) {
    _Not = Not;
    notDegisti(_Not);
  }
}

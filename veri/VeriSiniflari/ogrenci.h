#ifndef OGRENCI_H
#define OGRENCI_H

#include "temelverisinifi.h"

class Ogrenci : public TemelVeriSinifi {
    Q_OBJECT

public:
    typedef Ogrenci Veri;
    typedef std::shared_ptr<Veri> Ptr;

public:
    explicit Ogrenci(QObject *parent = nullptr);

    int ogrid() const;
    void setOgrid(int ogrid);

    int ogrno() const;
    void setOgrno(int ogrno);

    QString ogrAdi() const;
    void setOgrAdi(const QString &ogrAdi);

    QString ogrSoyadi() const;
    void setOgrSoyadi(const QString &ogrSoyadi);

    QString ogrAdres() const;
    void setOgrAdres(const QString &ogrAdres);

    int sinifid() const;
    void setSinifid(int sinifid);

signals:
    void ogridDegisti(int ogrid);
    void ogrnoDegisti(int ogrno);
    void ogrAdiDegisti(const QString &ogrAdi);
    void ogrSoyadiDegisti(const QString &ogrSoyadi);
    void ogrAdresDegisti(const QString &ogrAdres);
    void sinifidDegisti(int sinifid);

private:
    int _ogrid;
    int _ogrno;
    QString _ogrAdi;
    QString _ogrSoyadi;
    QString _ogrAdres;
    int _sinifid;
};

#endif // OGRENCI_H

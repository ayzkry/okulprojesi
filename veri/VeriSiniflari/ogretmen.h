#ifndef OGRETMEN_H
#define OGRETMEN_H

#include "temelverisinifi.h"

class ogretmen : public TemelVeriSinifi {
    Q_OBJECT

public:
    typedef ogretmen Veri;
    typedef std::shared_ptr<Veri> Ptr;

public:
    explicit ogretmen(QObject *parent = nullptr);

    int ogretmenid() const;
    void setOgretmenid(int ogretmenid);

    int sicilno() const;
    void setSicilno(int sicilno);

    QString ogretmenAdi() const;
    void setOgretmenAdi(const QString &ogretmenAdi);

    QString ogretmenSoyadi() const;
    void setOgretmenSoyadi(const QString &ogretmenSoyadi);

    QString ogretmenAdresi() const;
    void setOgretmenAdresi(const QString &ogretmenAdresi);

signals:
    void ogretmenidDegisti(int ogretmenid);
    void sicilnoDegisti(int sicilno);
    void ogretmenAdiDegisti(const QString &ogretmenAdi);
    void ogretmenSoyadiDegisti(const QString &ogretmenSoyadi);
    void ogretmenAdresiDegisti(const QString &ogretmenAdresi);

private:
    int _ogretmenid;
    int _sicilno;
    QString _ogretmenAdi;
    QString _ogretmenSoyadi;
    QString _ogretmenAdresi;
};

#endif // OGRETMEN_H

#ifndef TEMELVERISINIFI_H
#define TEMELVERISINIFI_H

#include <QObject>

#include <QDate>
#include <QDateTime>
#include <QString>
#include <memory>

#include "veri_global.h"

class TemelVeriSinifi : public QObject
{
    Q_OBJECT
public:
    explicit TemelVeriSinifi(QObject *parent = nullptr);

public:
    typedef QString Metin;
    typedef unsigned long long IdTuru;
    typedef float ParaBirimi;
    typedef QDate Tarih;
    typedef QDateTime TarihSaat;
    typedef unsigned int IsaretsizTamsayi;
    typedef int Tamsayi;

    IdTuru id() const;
    void setId(const IdTuru &id);

signals:
    void idDegisti(IdTuru id);

private:
    IdTuru _id;

};

#endif // TEMELVERISINIFI_H

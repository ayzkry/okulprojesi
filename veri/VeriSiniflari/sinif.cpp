#include "sinif.h"

Sinif::Sinif(QObject *parent) : TemelVeriSinifi(parent) {}

int Sinif::sinifid() const { return _sinifid; }

void Sinif::setSinifid(int sinifid) {
  if (_sinifid != sinifid) {
    _sinifid = sinifid;
    sinifidDegisti(_sinifid);
  }
}

QString Sinif::sinifAdi() const { return _sinifAdi; }

void Sinif::setSinifAdi(const QString &sinifAdi) {
  if (_sinifAdi != sinifAdi) {
    _sinifAdi = sinifAdi;
    sinifAdiDegisti(_sinifAdi);
  }
}

int Sinif::ogretmenid() const { return _ogretmenid; }

void Sinif::setOgretmenid(int ogretmenid) {
  if (_ogretmenid != ogretmenid) {
    _ogretmenid = ogretmenid;
    ogretmenidDegisti(_ogretmenid);
  }
}

int Sinif::yil() const { return _yil; }

void Sinif::setYil(int yil) {
  if (_yil != yil) {
    _yil = yil;
    yilDegisti(_yil);
  }
}

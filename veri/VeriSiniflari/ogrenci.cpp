#include "ogrenci.h"

Ogrenci::Ogrenci(QObject *parent) : TemelVeriSinifi(parent) {}

int Ogrenci::ogrid() const { return _ogrid; }

void Ogrenci::setOgrid(int ogrid) {
  if (_ogrid != ogrid) {
    _ogrid = ogrid;
    ogridDegisti(_ogrid);
  }
}

int Ogrenci::ogrno() const { return _ogrno; }

void Ogrenci::setOgrno(int ogrno) {
  if (_ogrno != ogrno) {
    _ogrno = ogrno;
    ogridDegisti(_ogrno);
  }
}

QString Ogrenci::ogrAdi() const { return _ogrAdi; }

void Ogrenci::setOgrAdi(const QString &ogrAdi) {
  if (_ogrAdi != ogrAdi) {
    _ogrAdi = ogrAdi;
    ogrAdiDegisti(_ogrAdi);
  }
}

QString Ogrenci::ogrSoyadi() const { return _ogrSoyadi; }

void Ogrenci::setOgrSoyadi(const QString &ogrSoyadi) {
  if (_ogrSoyadi != ogrSoyadi) {
    _ogrSoyadi = ogrSoyadi;
    ogrSoyadiDegisti(_ogrSoyadi);
  }
}

QString Ogrenci::ogrAdres() const { return _ogrAdres; }

void Ogrenci::setOgrAdres(const QString &ogrAdres) {
  if (_ogrAdres != ogrAdres) {
    _ogrAdres = ogrAdres;
    ogrAdresDegisti(_ogrAdres);
  }
}

int Ogrenci::sinifid() const { return _sinifid; }

void Ogrenci::setSinifid(int sinifid) {
  if (_sinifid != sinifid) {
    _sinifid = sinifid;
    sinifidDegisti(_sinifid);
  }
}

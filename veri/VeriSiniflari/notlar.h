#ifndef NOTLAR_H
#define NOTLAR_H

#include "temelverisinifi.h"

class notlar : public TemelVeriSinifi {
    Q_OBJECT

public:
    typedef notlar Veri;
    typedef std::shared_ptr<Veri> Ptr;

public:
    explicit notlar(QObject *parent = nullptr);

    int notid() const;
    void setnotId(int notid);

    int dersid() const;
    void setDersid(int dersid);

    int ogrid() const;
    void setOgrid(int ogrid);

    int Not() const;
    void setNot(int Not);

signals:
    void notidDegisti(int notid);
    void dersidDegisti(int dersid);
    void ogridDegisti(int ogrid);
    void notDegisti(int Not);

private:
    int _notid;
    int _dersid;
    int _ogrid;
    int _Not;
};

#endif // NOTLAR_H

#ifndef DERSLER_H
#define DERSLER_H

#include "temelverisinifi.h"

using namespace std;

class Dersler : public TemelVeriSinifi {
    Q_OBJECT

public:
    typedef Dersler Veri;
    typedef std::shared_ptr<Veri> Ptr;

public:
    explicit Dersler(QObject *parent = nullptr);

    int dersid() const;
    void setdersId(int dersid);

    QString dersAdi() const;
    void setDersAdi(const QString &dersAdi);

    int dersYili() const;
    void setDersYili(int dersYili);

    int donem() const;
    void setDonem(int donem);

signals:
    void dersidDegisti(int id);
    void dersAdiDegisti(const QString &dersAdi);
    void dersYiliDegisti(int dersYili);
    void donemDegisti(int donem);

private:
    int _dersid;
    QString _dersAdi;
    int _dersYili;
    int _donem;
};

#endif // DERSLER_H

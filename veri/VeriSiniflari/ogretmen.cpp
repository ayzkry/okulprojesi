#include "ogretmen.h"

ogretmen::ogretmen(QObject *parent) : TemelVeriSinifi(parent) {}

int ogretmen::ogretmenid() const { return _ogretmenid; }

void ogretmen::setOgretmenid(int ogretmenid) {
  if (_ogretmenid != ogretmenid) {
    _ogretmenid = ogretmenid;
    ogretmenidDegisti(_ogretmenid);
  }
}

int ogretmen::sicilno() const { return _sicilno; }

void ogretmen::setSicilno(int sicilno) {
  if (_sicilno != sicilno) {
    _sicilno = sicilno;
    sicilnoDegisti(_sicilno);
  }
}

QString ogretmen::ogretmenAdi() const { return _ogretmenAdi; }

void ogretmen::setOgretmenAdi(const QString &ogretmenAdi) {
  if (_ogretmenAdi != ogretmenAdi) {
    _ogretmenAdi = ogretmenAdi;
    ogretmenAdiDegisti(_ogretmenAdi);
  }
}

QString ogretmen::ogretmenSoyadi() const { return _ogretmenSoyadi; }

void ogretmen::setOgretmenSoyadi(const QString &ogretmenSoyadi) {
  if (_ogretmenSoyadi != ogretmenSoyadi) {
    _ogretmenSoyadi = ogretmenSoyadi;
    ogretmenSoyadiDegisti(_ogretmenSoyadi);
  }
}

QString ogretmen::ogretmenAdresi() const { return _ogretmenAdresi; }

void ogretmen::setOgretmenAdresi(const QString &ogretmenAdresi) {
  if (_ogretmenAdresi != ogretmenAdresi) {
    _ogretmenAdresi = ogretmenAdresi;
    ogretmenAdresiDegisti(_ogretmenAdresi);
  }
}

